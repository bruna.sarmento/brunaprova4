
height = float(input('Altura(m): '))
weight = float(input('Peso(kg): '))
	
import module
bmi = module.boryMassIndex(height,weight)

if bmi <= 17:
	print('IMC = ',round(bmi,2))
	print( 'Situação: Muito abaixo do peso')

elif 17.1 < bmi <= 18.5:
	print('IMC = ',round(bmi,2))
	print( 'Situação: Abaixo do peso')

elif 18.6 < bmi <= 24.9:
	print('IMC = ',round(bmi,2))
	print( 'Situação: Peso normal')

elif 25 < bmi <= 29.9:
	print('IMC = ',round(bmi,2))
	print( 'Situação: Acima do peso')

elif 30 < bmi <= 34.9:
	print('IMC = ',round(bmi,2))
	print( 'Situação: Obesidade grau I')

elif 35 < bmi <= 39.9:
	print('IMC = ',round(bmi,2))
	print( 'Situação: Obesidade grau II')

else:
	print('IMC = ',round(bmi,2))
	print( 'Situação: Obesidade grau III')